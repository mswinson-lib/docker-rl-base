.PHONY: clean build style test deploy

REPO=mswinson
NAME=rl-base
VERSION=v0.1.0

clean:
	docker rmi $(REPO)/$(NAME):$(VERSION)

build:
	packer build -var-file=variables.json -var image_tag=$(VERSION) template.json

style:
	packer validate -var-file=variables.json template.json

deploy:
	docker login -u $(DOCKER_USER) -p $(DOCKER_PASS)
	docker push $(REPO)/$(NAME):$(VERSION)

