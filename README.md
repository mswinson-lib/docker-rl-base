# docker-rl-base

RLglue base image  

[![](https://images.microbadger.com/badges/image/mswinson/rl-base.svg)](https://microbadger.com/images/mswinson/rl-base "Get your own image badge on microbadger.com")
[![](https://images.microbadger.com/badges/version/mswinson/rl-base.svg)](https://microbadger.com/images/mswinson/rl-base "Get your own version badge on microbadger.com")

## Prerequisites

      packer


## Configuration

      os: ubuntu:16.04
      packages:
          rlglue-core
          rlglue-c-codec

## Build

      git clone git@bitbucket.org:mswinson-lib/docker-rl-base
      cd docker-rl-base
      make build


## Usage

      docker run -it --rm mswinson/rl-base:latest /bin/bash

